---
title: "CloudflareDevDay2020"
date: 2020-03-03T16:23:01+11:00
lastmod: 2020-03-03T16:23:01+11:00
tags : 
    - dev
    - Web
    - Website
    - AWS
    - Hosting
categories : 
    - Web
description: "CloudflareDevDay2020"
highlight: false
draft: false
---

## CloudflareDevDay2020

In mid-February, I had the privilege to attend the first Melbourne Cloudflare dev event. This was just one of a series of sessions they ran across the country to reach out to developers and help educate people around their thinking and the direction of their efforts, along with showing off new features and ways to get the best out of existing ones.

Tickets for under $30 via Eventbrite and a day booked off from the office and I was away. It was more of a smaller intermit event and that allowed us to direct the flow of the day towards what interested us as a group.

The day was split into a basic set of sessions/breaks with lunch provided. We had staff from all over APAC visit us on the day. We had the pleasure of a visit from Raymond Maisano (Head of APAC), Nick Toulson (The local Account/Specialist) along with a good number of great technical guys, some that flew in from other countries to present. They dropped in to talk with us about their view for the future of the APAC region and where their efforts are placed.

But enough of all that let's get down to the technical stuff and what's new with Cloudflare.

To kick off the day we started with a run-through of the console, how to manage users in an account. Accounts vs zones and finally moving through the different options.
I would assume most people are familiar with the basics of what Cloudflare does, if not go grab a free account and try it out.

The new stuff!

    - Bot Fighting
    - CSAM Scanning Tool
    - Improvements to workers
    - lots of other stuff

Of particular note, there are the Bot fighting and CSAM (The Child Sexual Abuse Material) tools. Both are automated systems that monitor the traffic and pages requested on your site and use very precisely tuned systems to stop bad things. If you have user-uploaded content of any type I would recommend the CSAM tool as it costs nothing to turn on but could save many people lots.

Later in the day, we progressed through caching, the life and path of a request, SSL/TLS, firewall/WAF/Access/GeoIP and what applies first, Page rules (some great tips there), updates to the API, and lastly workers.

If you're not aware Cloudflare has recently updated the way you can access its API for doing most actions. They have also added a little API link in the console to help you work out what API to use for what action.  

Along with that they have published updated Python and Go libraries on their GitHub and reworked how you control API keys and authentication via their tools.
By allowing us to break down auth levels for each key, we can now start to write more targeted automation with least privilege in mind.

Lastly was the workers, if you don't know a Cloudflare worker is a javascript function running at the edge. You get access to all requests coming and going. Cloudflare has recently upgraded their HTML rewrite engine and other features to nicely flesh out what is possible when it comes to the edge. They also have a great library of templates and documents to get started with, everything from updating a HTTP header to injecting AB testing.

The workers can be controlled/managed via either serverless or Cloudflare's wrangler. I enjoyed using the serverless platform myself but it also has a few gotchas so I would say just work with whatever you are more comfortable with, however, if you are new to both tools aim for Wrangler for Cloudflare stuff as most of their development efforts go in that direction. You also have the option to just use the raw API's via one of the libraries or raw HTTP requests, their API docs have Curl examples for most requests allowing you to understand how each request is built and what formats are expected.

I ran a few experiments with setting up some extra HTTP security headers a while back and the process was simple and pretty direct.
I will do a quick rundown on how my serverless template came out here.

First Install Serverless

```YAML
    # Install the serverless cli
    npm install -g serverless
```

Then build our your `yaml` template.
Cloud flare have a quick start template you can pull using `serverless create --template cloudflare-workers --path new-project`

Any one familiar with serverless should be able to spot the simple changes I had to do, to get to here.

 ```YAML
 service:
    name: security-headers
    config:
        accountId: ${env:CLOUDFLARE_ACCOUNT_ID}
        zoneId: ${env:CLOUDFLARE_ZONE_ID} 


    provider:
    name: cloudflare
    stage: prod

    plugins:
    - serverless-cloudflare-workers

    functions:
    security-headers:
        name: security-headers
        script: security-headers # there must be a file called security-headers.js
        events:
        - http:
            url: '*example.net/*'
            method: GET
```

The biggest thing is the `url` setting as this has to be set per URL you want. You also need the wildcard at the start if you wish to match subdomains and http/s.

So in closing,
You can see with a very simple template you can deploy and manage multiple workers, making life very easy when you are trying to do AB testing or inject other things at the edge.
You can also access all of these features from the console and use their embedded editor to update your scripts. but that lacks the version control we all love.
Along with the other improvements, they launched Cloudflare is once again proving they are not scared to push towards the bleeding edge of what they can do when shipping features to clients.

Thanks for reading,
Lucas
