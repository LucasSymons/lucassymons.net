---
title: HelloWorld
date: 2019-08-24T16:22:08+10:00
lastmod: 2019-08-24T16:22:08+10:00
draft: false
categories: 
    - Web
tags: 
    - Website
    - Hugo
    - Hosting
    - AWS
    - CloudFlare
description: "Hello World, my first post!"
---

# My first post on the new site! 

I will be making some posts about how I put it all together and some other cool tips on setting stuff up. <br>
Along with any other techy stuff that takes my fancy. <br>
The basic break down is: <br>

- Site generator is [Hugo](https://gohugo.io/). 
- The Theme I went with is [Dream-Plus](https://github.com/UtkarshVerma/hugo-dream-plus)
- AWS S3 is hosting the static site
- Cloudflare free tier is doing CDN for me at the front end keeping costs down
- I used AWS R53 to register the domain but CloudFlare as my name servers, free name servers are awesome.
- Cloudflare gives out free SSL
- Google gives out free analytics, also CloudFlare has great stats

So all in all my yearly bill for the entire site is roughly

- $11 for domain registration
- $5 for hosting in S3

I host a few sites in S3 and my monthly AWS Bill is $0.80
They are not high traffic sites, the cost would go up with more traffic, but only once I passed the S3 free tier.

All in all, it's a really easy and cheap way, to get a site onto the internet with a little know-how.

Well, first post done! <br>
Thanks for reading.
