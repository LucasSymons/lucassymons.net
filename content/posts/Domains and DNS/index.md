---
title: Domains and DNS
date: 2019-08-30T16:52:39+10:00
lastmod: 2019-08-30T16:52:39+10:00
draft: false
categories:
    - Web
    - Domains
tags:
    - Web
    - Domains
    - DNS
    - DNSSec
    - Security
description: "What I look for in domains and DNS"
---

# Getting started with domains and DNS

So this is the start of a few posts about how I set up my website, the first thing you want to get sorted is the domain. Well, it's the first thing I like to get sorted. <br>
It is easy to spend hundreds on domains and DNS, so I went and found the cheapest domains I could. <br>

There are two parts to a domain, the registration and the DNS. You also need to be able to renew your domain registration and manage DNS. <br>

Optional is security, now there are some great security features you can get with a domain and a lot of people tend to ignore the options they have. <br>
You have to remember that whoever controls the domain/DNS controls all the bases. <br>
I started paying a bit more attention myself after watching this great talk on YouTube from DefCon, [DNS May Be Hazardous to Your Health]( https://youtu.be/ZPbyDSvGasw ) <br>
I highly recommend giving it a watch! <br>

## Domain Registration

Now the first thing people think of with domain registration is normally branding or cost. Some people care about one more then the other, for me it was the cost. <br> 
I wanted the right domain but I also wanted it for the right price, there are also limits put in place on some [TLDs]( https://en.wikipedia.org/wiki/Top-level_domain ) about who can register one and other things. <br>
For these reasons, I like to stick to the more generic or original TLDs like _.net_, _.com_, and _.org_ as they are fairly free an open around who can register them and are under the direct control of [ICANN]( https://www.icann.org/get-started ) although they are not the TLD Administrators. <br>

There are a few terms or phrases to understand about domains,

- Registrar: The company managing your domain registration.
- Registrant: The person/company registering the domain.
- TLD Administrator: The entity/company that looks after the TLD.
- Name Server Host: The service providing the DNS resolvers/servers for the domain.
- Name server: The individual DNS resolvers/servers that host your DNS records.

So to become a domain registrant you need to register a domain with a registrar and they will forward that on to the TLD Administrator once it is confirmed. You then set up DNS hosting with a name server host and add the name server details to your registration, as the TLD admin has to update the records on their end. <br>

I chose a .net domain as this is just a personal website with no shop or other services provided, .net is also a nice global TLD with very little restrictions around who can register it. Originally I used a local Australian based registrar as they had great prices and local support is always nice. <br>
However, after a change in management and pricing, I have since moved over to AWS R53. <br>
At the time when I moved my domains over to R53, they were the cheapest I could find. However, since then CloudFlare [released their registrar](https://blog.cloudflare.com/cloudflare-registrar/) and it might be just a little cheaper. <br>

The things I look for in a registrar are pretty basic,

1. Price
2. Easy management and good support
3. _Who is_ privacy
4. DNSSec Support

Both Cloudflare and R53 meet these requirements however, it should be noted R53 only support DNSSec if you are **not** using R53 as your names servers.<br>
I use Cloudflare for my name servers as they are free, as such I was able to configure DNSSec.

### Price

Price is the total cost of registering the domain in this case, for a .net domain I pay $11 a year. Other TLD's may cost more, for example a .co is $25 and a .adult is $100 <br>
Feel free to shop around some registrars get better deals on some domains.

### Easy management and good support

When it comes to domains you don't do a lot with them, you register them and then basically forget about them. However, it is really easy to run into issues if a domain expires on you or you mess something up. So make sure you are comfortable with your registrar's processes and know-how to contact them if something goes wrong. Because domain changes can be measured in days not hours and if your site is offline while that happens it's not fun. <br>
Also if you have a large number of domains to manage, having an easy management console that has the features you want can be a game-changer. I used to manage 400+ domains at a previous job and the original tooling was not that great.  The management console was a nightmare and more than 1 outage was longer than necessary due to poor support.

### WHO IS privacy

The [WHO IS info](https://en.wikipedia.org/wiki/Domain_name#Technical_requirements_and_process) attached to your domain must be published online and all registrars must supply a way of looking this up. This must contain contact info for someone responsible for the domain in case the domain is sending spam emails or hosting malware. <br>
Now normally you are the one that is responsible for your domain and it didn't take long for scammers to start using bots to search who is records and get a massive amount of info on a person, their name, phone number, email address, and physical address in some cases. <br>
To combat this, who is privacy was an added feature some registrars started adding, essentially they replace your info with theirs in the record and if they are contacted about your domain they will pass the message along. This can cause issues with some other services like EV SSL's but as they are soon to be a thing of the past I always opt for who is privacy. It has slowly become more of a standard and most registrars will offer this service nowadays.

### DNSsec Support

DNSsec a great security feature that has been added to domains and DNS in the last few years, although not in full-blown support yet it is coming along nicely. <br>
Essentially it's a record that gets added to both the registrar and your name servers. This creates a link verifying your name servers are the correct name servers and any downstream DNS server can verify the DNS record they have received by checking the DNSsec records. This helps fight DNS poisoning and other DNS/Domain based attacks using cryptography. If you want to read up on it more check out the [WIKI page]( https://en.wikipedia.org/wiki/Domain_Name_System_Security_Extensions ).

## DNS and Name Servers

So to start this off, name servers are DNS servers, but not all DNS servers are name servers.<br>
Name servers are the DNS servers you delegate to be the source of truth for the DNS records in your domain. They must also be registered as name servers, you can't use just any old DNS server. <br>
[DNS](https://en.wikipedia.org/wiki/Domain_Name_System) is one of the major components that run the internet and most networks. It translates the domain name you type into an address bar, into an IP address for your device to send a request to. This is used for many/most applications as it is a lot easier to update a DNS record then it is to update a hardcoded IP address in an application if you want to change your IP. Now I'm not going to dig too far into DNS and how it works, things like TTL or SOA records etc. <br>
For this we will keep it to looking at good name servers, for me I chose Cloudflare as they supply free name servers for anyone that wants them and they have some great DNS features that are required for doing the kind of hosting I do in AWS S3. <br>
This great feature is called [Cname Flattening](https://support.cloudflare.com/hc/en-us/articles/200169056-Understand-and-configure-CNAME-Flattening) it's against the DNS RFC to use a Cname at the root of a domain. <br>
So you can't use `example.com > Cname > myhost.com` <br>
But this is required if you want to host things in AWS S3 as they don't provide an IP address, just a DNS address. To get around this Cname flattening was crated, AWS calls it an *alias* record, but it's the same thing. Essentially when you request `example.com` CloudFlare will go and get the IP address for `myhost.com` and return the IP address as an *A* record not a *Cname* record. <br>
This allows you to get around the DNS RFC and be compliant while still been completely serverless and using a service like AWS S3.

<br>

Well, that about wraps up this post, as I haven't found anyone else that offers free Name Servers and DNS hosting.
I will do a follow-up post about how I configure DNS, this was more about domains.
<br>

Thanks for reading.
