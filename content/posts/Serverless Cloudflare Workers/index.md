---
title: "Serverless Cloudflare Workers"
date: 2019-12-07T19:37:23+11:00
lastmod: 2019-12-07T19:37:23+11:00
draft: false
categories: 
    - Web
    - Security
tags: 
    - Web
    - CloudFlare
    - Security
    - Serverless
    - CloudFlare Worker
description: Serverless deployments of Cloudflare workers.
---

# Serverless deployments of Cloudflare workers

In this post, I will be building on my previous post about [HTTP security headers](/posts/http-security-headers/) by explaining how to manage and deploy Cloudflare workers using the [serverless framework](https://serverless.com). <br>
If you're not familiar with the serverless framework it's a great tool used to manage deployments of all types of serverless functions to multiple providers like AWS, Azure, GCP, Cloudflare, etc. <br>
It will bundle your scripts/apps and any dependencies required, then deploy them all based on the config you provide in the `serverless.yaml` file. <br>

## Getting Setup
To get started you will need to install serverless either via NPM or your favorite package manager. <br>

*NPM*
```bash
    # Install the serverless cli
    npm install -g serverless
```
<br>

We are also going to use the template out of the [serverless quick start guide](https://serverless.com/framework/docs/providers/cloudflare/guide/quick-start/). <br>
Just run `serverless create --template cloudflare-workers --path new-project` <br>
This will set you up with most of what you need, but I will show you how to get auth to Cloudflare setup and stored in a semi-secure way. <br>
But first, we will start with the project. 
<br>

## Serverless JS script
As I outlined in my previous post I have been using the security headers script from Scott Helme, you can see it in his [Github repo](https://github.com/securityheaders/security-headers-cloudflare-worker), grab the contents of his `worker.js` file and copy it into the `helloWorld.js` file created by serverless, ensuring to overwrite any content already there. <br>
Then rename the `.js` file to something that makes more sense, like `security-headers.js`. You will also need to open the `package.json` file and update it with the new name of your script. <br>
Replace the value of `main` with your new script name, by default it is set to `index.js`. <br>
This tells NPM what file we are working with so it can build out our requirements. <br>

## Serverless YAML
You will need to edit your `serverless.yaml` file, it needs a name a function name and we will swap out the Cloudflare account into for environment variables so we can store the YAML file in git without saving our credentials in there. <br>
Once you are done it should look like the one below, be sure to set the `URL` part under the function to `*yourdomain.com/*` to set the route in Cloudflare. <br>

```yaml
    service:
    name: security-headers
    config:
        accountId: ${env:CLOUDFLARE_ACCOUNT_ID}
        zoneId: ${env:CLOUDFLARE_ZONE_ID} 


    provider:
    name: cloudflare
    stage: prod

    plugins:
    - serverless-cloudflare-workers
    
    functions:
    security-headers:
        name: security-headers
        script: security-headers # there must be a file called security-headers.js
        events:
        - http:
            url: '*example.net/*'
            method: GET

```
<br>

## Auth and ENV values
Now for Cloudflare and serverless, we need a few environment variables set. Two for the account info and two more for the API auth.
I suggest setting these up in another script file, but before you do, make sure you add it to your `.gitignore` first. <br>
I created an `auth.sh` file that looks like this, <br>

```bash
    export CLOUDFLARE_ZONE_ID=''
    export CLOUDFLARE_ACCOUNT_ID=''
    export CLOUDFLARE_AUTH_KEY=''
    export CLOUDFLARE_AUTH_EMAIL=''
    echo "Set cloudflare auth tokens"
```
<br>

Finding some of these values can be harder then it looks, Cloudflare still hasn't set them up in a nice central location. <br>
The Zone ID you will find on the [dashbord page](https://dash.cloudflare.com) after selecting the website you want to work with. <br>
scroll down and on the bottom right you will find a section titled `API` it will have both your `account Id` and `Zone id`. <br>
![Zone ID](/posts/serverless-cloudflare-workers/images/cloudflare-api-page.png)
<br>

Add these values to the `auth.sh` and jump over to your Cloudflare [profile API page](https://dash.cloudflare.com/profile/api-tokens) This is where you will 
find your Cloudflare `Global API Key`. Add that and the email address you use to login to Cloudflare to the `auth.sh` file. <br>

## Serverless Plugin's and Deployment
The last few steps are installing the Cloudflare serverless plugin and how to use the `auth.sh` script. 
<br>
To install the plugin simply run 
```
serverless plugin install --name serverless-cloudflare-workers
```
<br>

After that run `source ./auth.sh`, you may wonder why we used `source` here. This is to run the script in our current terminal process. If you just execute the script with `./auth.sh` it will spawn another terminal process to run it and the environment variables will not be set in your current terminal. <br>
Lastly, simply run `serverless deploy` to push your worker to Cloudflare have it start adding your security headers. 

## Note
The settings in the `worker.js` file from the git repo are very strict if it causes any issues with your site feel free to edit the first block and remove the part below. <br>

```
"Content-Security-Policy" : "upgrade-insecure-requests"
```
<br> 
I will soon make a post about how to get started with content security policies and reporting.
<br>

Thanks For Reading!
Lucas
