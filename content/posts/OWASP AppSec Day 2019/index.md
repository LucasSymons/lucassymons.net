---
title: "OWASP AppSec Day 2019"
date: 2019-11-01T11:23:17+11:00
draft: false
categories: 
    - Web
    - Security
tags: 
    - Web
    - Security
    - OWASP
    - Conference
description: OWASP AppSec Conference 2019
---


# OWASP AppSec Day 2019

This was to be my first [OWASP AppSec day]( https://appsecday.io/ ) so i was unsure of what to expect.
I would later learn that it would become one of my favourite conferences, especially about security.

## Getting Started

![Circle Ven](/posts/owasp-appsec-day-2019/images/keynote.jpg)
It starts like any normal I.T. conference, lots of coffee and everyone standing around not quite sure what's going on. 
However, they kicked it off with a bang in the form of their keynote speech from [Tanya Janca]( https://twitter.com/shehackspurple )!
Tanya managed to give an exciting and engaging talk about how everyone is security and we should all work to make everything more secure. 
She also threatened to stare at me for 9min if I didn't raise my right hand and swear to not break builds unless I had to, so Dev's you can feel safe for another day.

For those not in the know, Tanya is a Canadian AppSec enthusiast and community leader. Starting her local OWASP group, a [Women in Security]( https://twitter.com/WoSECtweets ) foucesed group, 
a monday mentor program on twitter, and just recently starting her own security company [Sidekick]( https://www.securitysidekick.dev/ )
Tanya was a charismatic and genuine talker, her excitement and enthusiasm for what she does is clearly visible and very engaging.

## All the Sessions

![Schedules](/posts/owasp-appsec-day-2019/images/schedule.png)
After the keynote, the 'Tracks' started there were four on offer this year, you can see the full list here, I will just talk about the ones I visited.

I started off with "Hunting bugs to extinction with static analysis" By [Paul Theriault]( https://twitter.com/creativemisuse ) from Mozilla.
It was an interesting talk outlining how a need for static code analysis came about and how it helped them scale out bug fixes across all app's not just
the ones owned by their team.Finding bugs in one app would improve the security off all their app's.


Up next was a super inventive talk named "How do I content security policy?" by [Kirk Jackson]( https://twitter.com/kirkj ) from Redshield.
I love his delivery style, he turned his talk into a "chose your own adventure" book that the room was able to slowly navigate around until we finished all his slides.
It was a great way to add some interaction to the talk and keep you engaged in what some would consider a boring topic.
I truly enjoyed his content and got some great pointers about how audit and track content security policy violations and compile that data to help you build/review your policy. 
I have yet to ask him, but I am also wondering if you could log it and do anomaly detection to help alert when someone is trying to launch an attack.


Lunch = More free coffee and some surprisingly great options for everyone around food. It was all hot and fresh and very enjoyable.


After lunch, I was able to catch a talk from the [Cruise Automation]( https://getcruise.com/ ) guys around the new Kubernetes security tool they have just open-sourced, called [K-Rail]( https://github.com/cruise-automation/k-rail ). 
If you use Kubernetes I would highly recommend giving it a look, it provides fast and detailed info to the end-user if a deployment violates a policy. 
He also had the live demo god's on his side and was able to successfully show off his tool in action and how to do some basic config.


Finishing out the after lunch block was, "When Bots attack - Mischievous puppets and stolen treasures" by [Andrew Logue]( https://au.linkedin.com/in/andrew-logue-0bb42510 ) from REA Group. 
It was a great talk around bot control and stopping content scrapers. It also tied in with highlighting the new tooling they are using called Kasada. The Kasada guys were able to do some great demos around stopping bots while not impacting real users. They also illustrated why it can be so hard to stop bots by showing how easy it was to spread his little bot over 2000+ IP's, hundreds of User Agents and do smart rate limiting.

## Homeward Bound

That rounds out my day, I spent the afternoon chatting with the vendors on-site like Checkpoint and [Hackerone]( https://www.hackerone.com/ ) and checking out the lock picking room. A lot of great products on display and Checkpoint looks to be doing some interesting stuff in the WAF space. Was well worth taking the time to have a look around.

A great day with many great people, I can't believe how complete the entire experience was, OWASP events have moved up to "must see!" status. Was also a very reasonable price if you just wanted to do the one day conference.
So great talks, vendors, and coffee! Lots of great initiatives around getting young people and women/girls into AppSec. The entire day went smoothly and I had a great day.

Hopefully, I see you there next year!

Thanks For Reading!
Lucas