---
title: "Bitbucket Piplines and tools"
date: 2020-01-29T14:46:06+11:00
lastmod: 2020-01-29T14:46:06+11:00
tags : 
    - dev
    - hugo
    - Web
    - Website
    - AWS
    - Hosting
categories : 
    - Web
description: "Bitbucket Piplines and tools"
draft: false
---

# Bitbucket Pipelines

Bitbucket offers a limited amount of pipeline minutes for free each month. As my build and deploy job is roughly 62 seconds ever with a sonaqube scan 50min is more than enough.

To get started you need to activate Bitbucket pipelines in your account and on that specific repo. To do this log in, open the repo page. hit settings and down the bottom you will find pipelines, just follow the prompts and away you go. 

## What is a pipeline

For any that haven't had a chance to play around with them yet.
It is essentially a step by step process for building or deploying your app. Some are purely scripts that run one after the other. Others give you access to a wide range of services and API's to use with each step and templated actions that are summed up in a few lines of code. 

Bitbucket is more like the latter, lots of templated and packaged actions to take advantage of in your pipelines. However there seems to be limited support for Hugo in the pre-built stuff so in a few quick lines we can spin up an alpine container, install what we need and kick off a deployment.

## Hugo Deploy

To get started make sure you have some Hugo deploy targets enabled, these are targets you define in the config.toml of your Hugo site that allows you to use the `Hugo deploy` command to kick off a deployment.
You can read more on it [here on the Hugo site](https://gohugo.io/hosting-and-deployment/hugo-deploy/), I set up just a single target been my S3 bucket so I don't need to name a target, but you can set up different environments if you want to do dev/uat/prod deployments based on branch, etc.  

## Static code analysis

The last thing I will cover in this post is bitbucket's great integration with the static code tool [Sonarcloud](https://sonarcloud.io/). Sonarcloud is a great tool to use for scanning your projects for bugs, code smells, test coverage, and vulnerabilities. I highly recommend having a look at their product. If you are running a public repo scanning is free!



In the next post, I will cover putting all these tools together into a pipeline and automating builds. 

Thanks for reading, 
Lucas


