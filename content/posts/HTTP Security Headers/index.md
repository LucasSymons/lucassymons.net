---
title: HTTP Security Headers
date: 2019-08-26T13:10:01+10:00
lastmod: 2019-08-26T13:10:01+10:00
draft: false
categories: 
    - Web
    - Security
tags: 
    - Web
    - CloudFlare
    - Security
    - HTTP
    - Serverless
    - CloudFlare Worker
description: http security headers, a serverless setup
---

# Security Headers in HTTP

So in the last few years, security headers in HTTP/s have been becoming more and more important. Mozilla has been [blogging]( https://blog.mozilla.org/security/2013/12/12/on-the-x-frame-options-security-header/ ) about this since at least 2013. <br>
In more recent times a few projects have started popping up to help advise about and analyze these headers. [Mozilla Observatory](https://observatory.mozilla.org/) and [Security Headers](https://securityheaders.com/) are the first two that jump to mind but many great sites will check them for you.

## So what are HTTP headers

HTTP headers are built into the HTTP protocol and are normally handled at the server level, not in the code. However, it is possible to modify them via code. <br>
The [HTTP RFC](https://tools.ietf.org/html/rfc2616) outlines them as such. <br>

>4.2 Message Headers
>
>   HTTP header fields, which include general-header (section 4.5),
>   request-header (section 5.3), response-header (section 6.2), and
>   entity-header (section 7.1) fields, follow the same generic format as
>   that given in Section 3.1 of RFC 822 [9]. Each header field consists
>   of a name followed by a colon (":") and the field value. Field names
>   are case-insensitive. The field value MAY be preceded by any amount
>   of LWS, though a single SP is preferred. Header fields can be
>   extended over multiple lines by preceding each extra line with at
>   least one SP or HT. Applications ought to follow "common form", where
>   one is known or indicated, when generating HTTP constructs, since
>   there might exist some implementations that fail to accept anything
>   beyond the common forms.

## How to add these headers to a Cloudflare site for free

So I am using Cloudflare as the CDN for my site and AWS S3 for the hosting. S3 is great but sadly it has no support for HTTP security headers. <br>
To get around this issue I have taken advantage of [CloudFlare's free tier worker]( https://blog.cloudflare.com/just-write-code-improving-developer-experience-for-cloudflare-workers/ ) that they have started providing and [a great blog from Scott Helme]( https://scotthelme.co.uk/security-headers-cloudflare-worker/ ). <br>
It is as easy as he makes it out to be, I will provide a quick list that proves how easy it is.

1. Log in to the Cloudflare control panel
2. Click on Workers
3. Click "Launch Editor"
4. Click "Add script" on the left
5. Fill in the script name and save
6. Click "edit" next to the name of the new script you created
7. Paste in the code from [Scott's GitHub]( https://github.com/securityheaders/security-headers-cloudflare-worker/blob/master/worker.js ) and save
8. Click "Dashboard" in the top left to go back to the worker's page
9. Click "Add Route"
10. Enter in your website URL and `/*` at the end, then select you new worker from the drop-down, select save.

That's it! <br>
Great HTTP security headers added to your site in **10** steps! <br>

I will make some follow up posts about tuning the headers and the config I use for Cloudflare but wanted to get this up to say thanks to Scott for making life so easy! <br>

Thanks for reading.
