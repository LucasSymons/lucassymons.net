---
title: Adding DNSsec to R53
date: 2019-10-04T20:35:02+10:00
lastmod: 2019-10-04T20:35:02+10:00
draft: false
categories: 
    - DNS
    - Domains
tags: 
    - Route53
    - AWS
    - DNS
    - DNSsec
    - Security
description: Adding DNS to a route 53 registered domain
---

# Adding DNSsec to a route 53 registered domain

**Note you can't add DNSsec to a domain using R53 as name servers** <br>
[Source](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/domain-configure-dnssec.html)

## Getting started

Over in my post about [domains and DNS]({{< ref "posts/Domains and DNS" >}}) I talked about what DNSsec is, but here I want to show you how to set it up in AWS Route 53. If you want to do some deeper dives into what it is and how it's used, the [wiki page](https://en.wikipedia.org/wiki/Domain_Name_System_Security_Extensions) is fairly complete. <br
The basic outline is; 

- Name Server Provider issues key and provides encryption details
- We supply this to the Domain Registrar and they publish it in our domain
- When a client requests a DNS record they can check the key matches between both the name servers and Registrar info.

## Generating the Key

I will be using Cloudflare for my name servers but this process and the details you need should be fairly similar across name server providers. <br>
In the Cloudflare console open up to the *DNS* heading and scroll down to *Enable DNSsec* this will bring up a list of details about the DNSsec record you need to publish into your registrar's records.
You will also want to open up your AWS Console side by side and jump into the registered domains part of Route 53. <br>

## Adding the Key to R53 (console)

Once you have the R53 console open, select the domain you would like to manage that matches the one you started the config on in Cloudflare. <br>
Over to the left you will see an option for *DNSSEC status* under that is an option to *Manage Keys* <br>

This option will allow you to add a new DNSsec key to the domain. <br>
In R53 the following info translates from Cloudflare,

- Key Type (R53) = Flags (Cloudflare)
- Algorithm (R53) = Algorithm (Cloudflare)
- Public Key (R53) = Public key (Cloudflare)

With the Algorithm you could be able to just match the numbers, In cloudflare mine shows `13` in R53 it shows as `13 - ECDSAP256SHA256` <br>
Flags (Cloudflare) is the hardest one to match with `Key Type` in R53. But once you have copied over the relevant info you can just hit apply in R53. <br>
You should then be able to see the *Add DNSSEC* Job under *Pending Requests* in R53, feel free to hit confirm in Cloudflare to kick off their record creation.
Wait 10-15 min and Cloudflare should show DNSsec as active.

### AWS CLI Notes

As of writing there are no options to update DNSsec via the CLI it's a console only type thing.
The only reference to it I can find in the doc's is [here](https://docs.aws.amazon.com/cli/latest/reference/route53domains/get-domain-detail.html) if you scroll down you can find the part. <br>

> DnsSec -> (string)  <br>
>>  Reserved for future use.

If I find a way to do it in the future, I will post an update to this post.

<br>
This wraps up this post, the next one I have planned will be around some serverless stuff.
<br>

Thanks for reading.
