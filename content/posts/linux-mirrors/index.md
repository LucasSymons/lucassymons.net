---
title: "Linux Mirrors"
date: 2020-12-18T13:20:07+11:00
lastmod: 2020-12-18T13:20:07+11:00
tags : 
    - dev
    - Web
    - Website
    - Hosting
categories : 
    - Web
description: "Linux Mirrors"
draft: false
---

## Linux Mirrors and how I set one up
<br>
I wanted to put out a post explaining what a Linux mirror is and how I set one up.
A Linux mirror is a web server that holds a "mirror" copy of the distro's packages.
So if a Linux machine needs to pull updates or new packages they can contact a mirror server or the main server to pull
these binaries. This helps distribute the load so the distro can support a larger member base without been the sole source of all their packages.
Mirrors are normally community-run or sponsored, a lot of educational institutions offer free mirror hosting to help out the Linux communities. 
Also, some ISP's will offer to host a mirror as they also benefit by keeping the data usage inside their network.<br>

The distro I choose to use, Manjaro was set up in this very classic way. But unfortunately out of the two mirrors set up inside Australia, both
were having issues. One was completely non-responsive and the other was very patchy. I had been looking for an opportunity to give back to the
Linux community as I enjoy all the free products they offer and use them every day. The usual way to give back to the Linux community is to work
on the tools and programs every one makes use of. You can offer development skills, problem-solving or even just writing documentation it all helps the community grow and support its self.<br>

Sadly I am not a great dev and as much as I offer tech support where I can, I felt like there had to be a way to make use of the Ops kills I have acquired over my career and use them to give back. Looking into the infrastructure used to run a distro I discovered a good mirror is one of the most valuable things you can offer. On top of that going through the issues Australia was facing with mirrors going offline it became an obvious choice to look at setting up one of my own.<br>

I started digging into the cheapest and most manageable options for offering a mirror. Talking with other mirror hosts I got some numbers around traffic that suggested maybe 500GB of traffic a month. Working with that number I found that Linode was very cheap and gave a great 4TB of free traffic.<br>

So I spun up a nginx server in Linode and added Cloudflare, lots of reading about Cloudflare's caching behaviour and how to manage it correctly I enabled a feature called origin cache control. <br>
This allows you to set cache headers at the webserver that control how long Cloudflare will cache objects. Once I had caching sorted I had to work on security stuff, there were two things I wanted. Firstly I wanted Fail2ban to be sure ssh was nice and locked down. 
Also when using Cloudflare it is best practise to lock your server down so it only accepts traffic from Cloudflare's IP's. I found a handy script I could add to cron to ensure that my IP tables stayed up to date with Cloudflare. I also did some other tweaks to lock things down and ensure things run smoothly. Technical write up to follow so you can find all the setup you need if you want to replicate. <br>

Once I was successfully syncing from the upstream Manjaro servers and it was all serving nicely I had some people from the Manjaro forums do some testing for me. That proved successful with a few little tweaks to allow Pacman (Arch/Manjaro's package manager) user agents to access everything, I raised a request with the Manjaro Dev's to get added to the official mirror list. <br>

Then I just had to wait and the traffic started flooding in. I was surprised as I was getting good speed ratings from Cloudflare worldwide so I was serving a lot of traffic from outside of Australia (my intended audience) so my traffic levels started creeping up at about 4TB served for the month I was starting to get worried I would get charged overage usage from Linode. But then I discovered that Linode is part of the Cloudflare bandwidth alliance. This means they provide free traffic if you're using Cloudflare in front of your service.<br>

A few quick chats with their support staff over IRC and a support ticket later they had assured me my overage would be refunded if I wen over-usage.<br>

So now I am serving 60-80TB of traffic a month with a cache hit ratio of about 75% and it's all nice and fast. I also later got myself added as an arch repo as it didn't cost me much extra for the disk space.<br>

technical write up's to come at a later date about how to config nginx, fail2ban, IP Tables for Cloudflare and others.<br>

Thanks for reading.