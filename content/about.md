---
title: About
date: 2019-08-30T16:52:39+10:00
include_toc: false
---

So, Who Am I?


I’m just another tech guy that likes playing around on the internet. 
Professionally I work in DevOps with a SysAdmin Background. I have a passion for the internet/Web and enjoy exploring the many cool things you can do with it. 


You can find my contact info at the top of this page if you need to know more or swap back to the <a href="https://lucassymons.net/posts"> posts </a> view to
check out what I have been up to lately.


Other Places you can find me!

- [Linux Australia]( https://linux.org.au ) I like to attend events and will hopfully soon get a chance to catch a LUG or HackerSpace meet.
- [Manjaro Forum]( https://forum.manjaro.org/ ) Manjaro is my distro of choice at the moment so you can find me on the forums helping where I can. 