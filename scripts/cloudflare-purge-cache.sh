#!/bin/bash
set -o errexit
#set -o pipefail
set -o nounset
# set -o xtrace

if [[ -z "${CF_API_KEY}" ]]; then
    printf "Missing Cloudflare API Key"
    exit 1
else
    API_KEY="$(echo "${CF_API_KEY}")"
fi

if [[ -z "${CF_ZONE}" ]]; then
    printf "Missing Cloudflare Zone Identifier"
    exit 1
else
    ZONE="$(echo "${CF_ZONE}")"
fi


export auth_header=$(echo "Authorization: Bearer ${API_KEY}")
export url="https://api.cloudflare.com/client/v4/zones/${ZONE}/purge_cache"
touch ./tmpFile
curl -X POST "${url}" -H "${auth_header}" -H "Content-Type: application/json" --data '{"hosts":["www.lucassymons.net","lucassymons.net"]}' -o ./tmpFile
sleep 1
success=$(cat ./tmpFile | jq '.success')
tempfile=$(cat ./tmpFile)
rm ./tmpFile

if [ $success != "true" ]; then
  printf "purge failed"
  printf "${success}"
  printf "${tempfile}"
  exit 1
else
  printf "purge successful"
fi