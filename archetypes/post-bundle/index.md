---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
lastmod: {{ .Date }}
tags : 
    - dev
    - hugo
    - Web
    - Website
    - AWS
    - Hosting
categories : 
    - Web
description: "{{ replace .Name "-" " " | title }}"
highlight: false
draft: true
---
